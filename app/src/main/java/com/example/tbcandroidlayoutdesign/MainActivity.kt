package com.example.tbcandroidlayoutdesign

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.tbcandroidlayoutdesign.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        backgroundAnimation()
        iconAnimations()

    }

    private fun backgroundAnimation() {
        val drawableAnim: AnimationDrawable = binding.constraintCont.background as AnimationDrawable
        drawableAnim.apply {
            setEnterFadeDuration(1300)
            setExitFadeDuration(3000)
            start()
        }
    }

    private fun iconAnimations() {
        var clickCounter = 1
        binding.apply {
            buttonRhino.setOnClickListener {
                binding.mainCardView.animate().apply {
                    duration = if (clickCounter % 2 == 0) {
                        rotationBy(360f)
                        1500
                    } else {
                        rotationBy(-360f)
                        1500
                    }
                    clickCounter++
                }.start()
            }
            buttonWhale.setOnClickListener {
                binding.buttonWhale.animate().apply {
                    rotationBy(360f)
                    duration = 1000
                }.start()
            }
            buttonLama.setOnClickListener {
                binding.buttonLama.animate().apply {
                    rotationBy(360f)
                    duration = 1000
                }.start()
            }
            buttonChameleon.setOnClickListener {
                binding.buttonChameleon.animate().apply {
                    rotationBy(360f)
                    duration = 1000
                }.start()
            }
            buttonCrab.setOnClickListener {
                binding.buttonCrab.animate().apply {
                    rotationBy(360f)
                    duration = 1000
                }.start()
            }
            buttonElephant.setOnClickListener {
                binding.buttonElephant.animate().apply {
                    rotationBy(360f)
                    duration = 1000
                }.start()
            }
            buttonFlaming.setOnClickListener {
                binding.buttonFlaming.animate().apply {
                    rotationBy(360f)
                    duration = 1000
                }.start()
            }
            buttonOwl.setOnClickListener {
                binding.buttonOwl.animate().apply {
                    rotationBy(360f)
                    duration = 1000
                }.start()
            }

        }
    }
}